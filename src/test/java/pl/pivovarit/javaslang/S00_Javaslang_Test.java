package pl.pivovarit.javaslang;

import javaslang.*;
import javaslang.collection.List;
import javaslang.control.Option;
import org.junit.Test;

import static javaslang.API.$;
import static javaslang.API.Case;
import static javaslang.API.Match;

/**
 * Grzegorz Piwowarek
 * gpiwowarek@gmail.com
 * @pivovarit
 * https://bitbucket.org/pivovarit/w4d_17_03_2017
 */
public class S00_Javaslang_Test {

    @Test
    public void e00() {

        Function2<Integer, Integer, Integer> sum = (i, i2) -> i + i2;
        Function1<Integer, Function1<Integer, Integer>> curried = sum.curried();

        final Function1<Integer, Integer> add2 = curried.apply(2);

    }

    @Test
    public void e01() {
        final Tuple2<String, Integer> t = Tuple.of("Java", 8);
        final Tuple2<String, Integer> map = t.map(String::toUpperCase, i -> i + 1);
    }

    @Test
    public void e02() {
        /*final List<Optional<String>> optionals = Arrays.asList(Optional.of("asd"), Optional.ofNullable(null));

        final List<String> collect = optionals
          .stream()
          .filter(Optional::isPresent)
          .map(Optional::get)
          .collect(Collectors.toList());*/
    }

    @Test
    public void e03() {
        final List<Option<String>> options = List.of(Option.of("asd"), Option.none());

        final List<String> strings = options.flatMap(o -> o);

        System.out.println(strings);

    }

    @Test
    public void e04() {
        Lazy.of(() -> {
            System.out.println("liczymy!");
            return "result";
        }).map(String::toUpperCase).get();

    }

    @Test
    public void e05() {
        Object s = "asd";
        Match(s).of(
          Case($(Predicates.is("asdasd"), "asd")),
          Case($(Predicates.is("asdsad"), "asd"))
        )    ;

    }
}