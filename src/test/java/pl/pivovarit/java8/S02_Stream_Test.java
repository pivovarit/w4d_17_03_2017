package pl.pivovarit.java8;

import org.junit.Test;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

/**
 * Grzegorz Piwowarek
 * gpiwowarek@gmail.com
 * @pivovarit
 * https://bitbucket.org/pivovarit/w4d_17_03_2017
 */
public class S02_Stream_Test {

    @Test
    public void e00() throws Exception {
        Stream.of("a");
        Stream.of("a", "b");

        final List<String> asd = Arrays.asList("asd");
        asd.stream();
    }

    @Test(expected = IllegalStateException.class)
    public void e01() throws Exception {
        final Stream<String> a = Stream.of("a");
        a.forEach(i -> {});
        a.forEach(i -> {});
    }

    @Test
    public void e02() throws Exception {
        final List<String> strings = Arrays.asList("a", "b", "c", "d");

        strings.stream()                        //1
          .map(s -> s.toUpperCase())            //2
          .map(s -> s + "post")                 //3
          .forEach(s -> System.out.println(s)); //4

    }

    @Test
    public void e03() throws Exception {
        final List<String> strings = Arrays.asList("a", "B", "C", "d");

        strings.stream()
          .map(s -> s.toUpperCase())
          .map(s -> s + "post")
          .filter(s -> !Objects.equals(s, "B")) // nie filtruje niczego!
          .forEach(s -> System.out.println(s));

    }

    @Test
    public void e04() throws Exception {
        final List<String> strings = Arrays.asList("a", "B", "C", "d");

        strings.stream()
          .map(s -> s.toUpperCase())
          .filter(s -> !Objects.equals(s, "B")) // już filtruje!
          .map(s -> s + "post")
          .forEach(s -> System.out.println(s));

    }


    @Test
    public void e05() throws Exception {
        final List<String> strings = Arrays.asList("a", "B", "C", "d");

        final boolean b = strings
          .stream()
          .map(String::toUpperCase)
          .anyMatch(s -> Objects.equals(s, "B"));

    }

    @Test
    public void e06() throws Exception {
        final List<String> strings = Arrays.asList("a", "B", "C", "d");

        strings.stream()
          .map(s -> s.toUpperCase())
          .filter(s -> Objects.equals(s, "A"))
          .map(s -> {
              System.out.println("wywolanie");
              return s;
          })
          .findFirst();
    }

    @Test
    public void e07() throws Exception {
        final List<String> strings = Arrays.asList("a", "B", "C", "d");

        strings.stream()
          .forEach(System.out::println);
    }

    @Test
    public void e08() throws Exception {
        final List<String> strings = Arrays.asList("a", "B", "C", "d");

        strings
          .forEach(System.out::println);
    }

    @Test
    public void e09() throws Exception {
        final List<String> strings = Arrays.asList("aaaa", "Basd", "C", "ddd");

        final List<String> list = strings
          .stream()
          .map(String::toUpperCase)
          .collect(toList());

        final Set<String> set = strings
          .stream()
          .map(String::toUpperCase)
          .collect(toSet());

        final LinkedList<String> linkedList = strings
          .stream()
          .map(String::toUpperCase)
          .collect(toCollection(LinkedList::new));

        final Map<String, Integer> collect = strings
          .stream()
          .map(String::toUpperCase)
          .collect(toMap(i -> i, String::length));

        final Map<Integer, List<String>> groupingBy1 = strings
          .stream()
          .map(String::toUpperCase)
          .collect(groupingBy(String::length));

        final Map<Integer, Set<String>> groupingBy2 = strings
          .stream()
          .map(String::toUpperCase)
          .collect(groupingBy(String::length, toSet()));

        final Map<Integer, String> groupingBy3 = strings
          .stream()
          .filter(shorterThan())
          .map(String::toUpperCase)
          .collect(groupingBy(String::length, reducing("", (s1, s2) -> s1 + s2)));

        strings
          .stream()
          .filter(s -> s.length() < 4)
          .map(String::toUpperCase)
          .collect(collectingAndThen(groupingBy(String::length, reducing("", (s1, s2) -> s1 + s2)), integerStringMap -> integerStringMap
          ));

        System.out.println(groupingBy3);

    }

    private Predicate<String> shorterThan() {
        return s -> s.length() < 4;
    }

    @Test
    public void e10() throws Exception {

        IntStream.iterate(0, i -> i + 1).boxed()
          .limit(100)
          .forEach(System.out::println);
    }

    @Test
    public void e11() throws Exception {
        IntStream.iterate(0, i -> i + 1).boxed().parallel()
          .limit(100)
          .forEach(System.out::println);
    }

    @Test
    public void e12() throws Exception {
        IntStream.iterate(0, i -> i + 1).boxed().parallel()
          .forEach(System.out::println);
    }

    @Test
    public void e13() throws Exception {
        final List<String> strings = Arrays.asList("aaaa", "Basd", "C", "ddd");

        final Map<Boolean, List<String>> collect = strings
          .stream()
          .collect(Collectors.partitioningBy(s -> s.length() < 2));

    }

    @Test
    public void e14() throws Exception {
        final List<String> strings = Arrays.asList("aaaa", "Basd", "C", "ddd");

        final String reduce = strings
          .stream()
          .reduce("", (s1, s2) -> s1 + s2);
    }



    private Consumer<String> wrapWithLogging(Consumer<String> consumer) {
        return s -> {
            consumer.accept(s);
            System.out.println("log");
        };
    }

    @Test
    public void e15() throws Exception {
        wrapWithLogging(s -> {}).accept("");
    }

    @Test
    public void e16() throws Exception {
        final List<String> strings = Arrays.asList("aaaa", "Basd", "C", "ddd");

        final String[] reduce = strings
          .stream()
          .toArray(String[]::new);
    }
}