package pl.pivovarit.java8;

import org.junit.Test;

import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Grzegorz Piwowarek
 * gpiwowarek@gmail.com
 * @pivovarit
 * https://bitbucket.org/pivovarit/w4d_17_03_2017
 */
public class S01_Optional_Test {

    @Test
    public void e00() throws Exception {

        String s = "sss";

        if (s == null) {
            if (s.toUpperCase() == null) {
            }
        }

        String result = Optional.ofNullable("")    //1
          .map(String::toUpperCase)   //2
          .map(s1 -> s1 + "postfix")  //3
          .orElse("default");    //4

        Optional<String> opto = null;
    }

    @Test
    public void e01() throws Exception {

        final String result = Optional
          .ofNullable("")
          .map(s -> s.toUpperCase())
          .filter(s -> !s.isEmpty())
          .map(s -> {
              System.out.println("hej");
              return s;
          })
          .orElse("default");

        System.out.println(result);
    }

    @Test
    public void e02() throws Exception {

        final String result = Optional
          .ofNullable("")
          .map(s -> (String) null)
          .filter(s -> !s.isEmpty())
          .orElse("default");

        System.out.println(result);
    }

    @Test
    public void e03() throws Exception {

        final String result = Optional
          .ofNullable("")
          .filter(s -> !s.isEmpty())
          .orElse(defaultString());

        System.out.println(result);
    }

    private String defaultString() {
        System.out.println("10min...");
        return "default";
    }

    @Test
    public void e04() throws Exception {

        final String result = Optional
          .ofNullable("")
          .filter(s -> !s.isEmpty())
          .orElseGet(() -> defaultString());

        System.out.println(result);
    }


    @Test(expected = IllegalStateException.class)
    public void e05() throws Exception {

        final String result = Optional.ofNullable("")
          .filter(s -> !s.isEmpty())
          .orElseThrow(IllegalStateException::new);
    }

    @Test
    public void e06() throws Exception {
        Optional.ofNullable("")
          .ifPresent(s -> System.out.println("pusty string!"));
    }

    @Test(expected = NoSuchElementException.class)
    public void e07() throws Exception {
        Optional.ofNullable(null)
          .get();
    }

    @Test
    public void e08() throws Exception {
        final Optional<String> optional = Optional.ofNullable("");

        if (optional.isPresent()) {
            optional.get();
        }
    }

    @Test
    public void e09() throws Exception {
        final Optional<String> adam = Optional
          .ofNullable("Adam")
          .flatMap(name -> findByName(name));

    }

    private Optional<String> findByName(String name) {
        return Optional.ofNullable("asd");
    }


    @Test
    public void e10() throws Exception {
        Optional.ofNullable(null);
    }

    @Test(expected = NullPointerException.class)
    public void e11() throws Exception {
        Optional.of(null);
    }


    private int calculate(Optional<Integer> i1, Optional<Integer> i2) {
        return i1.orElse(0) + i2.orElse(0);
    }
}