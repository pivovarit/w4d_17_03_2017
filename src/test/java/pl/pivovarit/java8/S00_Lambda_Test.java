package pl.pivovarit.java8;

import org.junit.Test;

import java.util.function.*;

/**
 * Grzegorz Piwowarek
 * gpiwowarek@gmail.com
 * @pivovarit
 * https://bitbucket.org/pivovarit/w4d_17_03_2017
 */
public class S00_Lambda_Test {

    @Test
    public void e00() throws Exception {

        /*
        i -> i + 1
        (i) -> {return i + 1;}
        (i, j) -> i + j
        () -> 42
        i -> {}
         */

    }

    @Test
    public void e01() throws Exception {
        Function<String, String> f = new Function<String, String>() {
            @Override
            public String apply(String s) {
                return s + 1;
            }
        };

        Function<String, String> f2 = s -> s + 1;
        Function<Integer, Integer> f3 = s -> s + 1;
    }

    @Test
    public void e02() throws Exception {

        Function<String, String> f = i -> i + 1;
        BiFunction<String, String, String> f2 = (i, j) -> i + j;

        Supplier<String> f3 = () -> "42";
        Consumer<String> f4 = System.out::println;

        Predicate<String> f5 = s -> s.isEmpty();
        UnaryOperator<Integer> f6 = i -> i + 1;
        BinaryOperator<Integer> f7 = (i, j) -> i + j;
    }

    @Test
    public void e03() throws Exception {

        Function<String, String> f = i -> i + 1;
        BiFunction<String, String, String> f2 = (i, j) -> i + j;

        Supplier<String> f3 = () -> "42";
        Consumer<String> f4 = System.out::println;

        Predicate<String> f5 = s -> s.isEmpty();
        UnaryOperator<Integer> f6 = i -> i + 1;
        BinaryOperator<Integer> f7 = (i, j) -> i + j;
    }

    @Test
    public void e04() throws Exception {
        Function<String, String> f = String::toUpperCase;
        BinaryOperator<Double> f2 = Double::sum;
    }



}